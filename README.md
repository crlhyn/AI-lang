# Scan-Digitize tools

These are a set of scripts to simplify the use of free tools:

  - OCR tessaurus 
  - spelling hunspell  and dictiories
  - gm image-magick

We assume a stable linux base CentOS RHEL Ubuntu ... with this tools installed (dnf apt yum install ...)

 
## usage / tool access-points 

1. can use with USB & File-Manager and terminal or script call to the utilities in ./bin/<toolname>
1. can leave also web apache /nginx as cgi-bin wrapper to the same tools localhost:8100/cgi/<toolname>
1. ideally, do integrate the tools in a moderm pipeline (Azure DevOps, Jira, cloud Git ...)

# What-For: Five examples solved using Linux tools (images below).
- Improve Russian text recognition by applying Linux zoom & bright filters
- Recognize Arabic text after splitting pages, and zooming (hard to automate)
- Extract figures from embedded PDFs, and make it monochrome before OCR 
- Upload files using WinSCP to Confidential or Secret sites (to evolve to WEB)
- Decreasing high resolution PDFs into bright A4/Letter can save time/space

Any of these cases matches your goals ? 
Which one would be a priority ?

The Linux-processed images can help OCR produce clear text, but has 2 risks:
- r1) need a technical person to update quarterly this Linux (PC or Virtual) 
- r2) free tools are user-unfriendly => users need patience until it evolves

## Improve Russian text recognition by applying Linux zoom & bright filters

- example1: OCR didn't recognize some Russian words in the Blured part of scanned image [^ou]
-  Linux tools applied: zoom and sharpen-filter on images before OCR
-  result: OCR was able to recognized the missing Russian text after zoomed-in

The 2 pages were converted to russian text easily:
 ![recognize Russian in zoom](images/rus1t.png "recognize Russian in zoom")
  *recognize Russian in zoom*

But few words on the right side of the 2nd page [^ou] needed to sharpen and bright filters to be recognized. 
 ![blur](images/rus2t.png "blur")


[^ou]:   In Search of the Miraculous. pg 134. fig 15 , Peter D. Ouspensky, Пётр Демьянович Успенский;  , published at Первая иллюстрация к книге В поисках чудесного - Петр Успенский https://www.labirint.ru/screenshot/goods/429033/1/ 


##  Recognize Arabic text after splitting pages, and zooming (hard to automate)

- example2: OCR Arabic, as mentioned, has stronger fail rate. Used a public text from [^su]
-  Linux tools applied: split page in pages, even zoom out by lines
-  risk:  Split pages in 2 can cut page wrongly  (zoom-in by lines will take long to automate)[^b]

 ![recognize arab in paragraph](images/arat.png "recognize arab in paragraph")
  *recognized arab in paragraph*

[^su]: Al-Ghazali, Letter To A Disciple pg24 "

[^b]: All this is for the standard Linux filters and basic automation. (More complex Automation, like zooming each line for recognition, would need a big project)


##  Extract figures from embedded PDFs, make it monochrome before OCR 

If we receive a PDF already digital, for instance see an public INIS document : [^nd]  
- problem:  how to scan one chart or graphic/figure embedded inside a page document ?
-  tool to apply: linux to separate the page 914 ;
   - then expand all images embedded inside the page
   - finally remove colors, leaving only black and white for the OCR to better recognize
- result: the text in the small picture (in color) is recognized & transformed into clear text  
  
  ![tools to extract figure and recognize it](images/PDFfigures.png	"tools to extract figure and recognize it")
  *tools to extract figure and recognize it*


[^nd]: NDM06 2. symposium on neutrinos and dark matter in nuclear physics https://inis.iaea.org/collection/NCLCollectionStore/_Public/39/005/39005128.pdf 
```bash

 pdftohtml pp-ndm-j914.pdf             # extracting images in the page 914                                            
Page-1

root# ~/Desktop/5d香 Гур echo > chart.txt && tesseract -l eng pp-ndm-j914-1_12.png chart && cat chart.txt 
Tesseract Open Source OCR Engine v4.0.0-beta.1 with Leptonica
Empty page!!
Empty page!!

### we need to filter scale and remove colors leaving only black and white, then it recognise the text:

root# ~/Desktop/5d香 Гур echo > chart.txt && tesseract -l eng p12.png chart && cat chart.txt 
Tesseract Open Source OCR Engine v4.0.0-beta.1 with Leptonica
FREJUS_NEMO 2

FREJUS_NEMO 3

.EJUS_SUPERNEMO

GERDA

MIBETA a
CUORICINO

x

CUORE

CANDELS

MAJORANA
```


##  Upload files using WinSCP to Confidential or Secret sites (evolve to WEB)

- standard putty or winscp tool to upload files
- yet the simplest is via WEB (to be developed, and shared)
The tools are straight forward for techs but how to bring it to users ?
- a proposal is to bring a set of 5-10 common filters in a web or usb/pc
- we can put a web in the   secure zone 

##  Decreasing high resolution PDFs into bright A4/Letter can save time/space

- The Linux tool *ghostscript* already do this and can the automated to process many pages or many files

For instance, almost 190MB file published by INIS [] NDM06: 2. symposium on neutrinos and dark matter in nuclear physics https://inis.iaea.org/collection/NCLCollectionStore/_Public/39/005/39005128.pdf was reduced to A4 basic resolution in just 31MB


 
```mermaid
graph TD;
  PDF --> image;
    PDF --> A4 --> PDF;
   Scan --> image-->Zoom;
  image-->Split-->OCR;
  Zoom-->Bright--> OCR--> TEXT;
  image-->Upload;
```


# ToC

_Table_

 [[_TOC_]]

<details>
<summary markdown="span"> - </summary>
 examples
</details>

---
a: 77
b: 67
 
...
