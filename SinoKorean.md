2023 AI-Languages updates for Sino-Korean and Tibetan (following 2020-21 works on Ukraine reports, and Arabic texts) 


## Lyrics Aak 

**Sino-Korean (Hanja):** 

	爲報冤仇千古重
	今秋發誓心意濃 
	道德法則傳聲教 
	日 月 山 川 萬古 同

**Romanized Transliteration:** 

	Wi bo yon chu chon ku chung 
	Im chu pal sweh shim i nong 
	Do tek pok chok chon song jo 
	Il mul san chon  man ku  tong

**Modern Korean (Hangul):** 

	위보원구천고중 
	금추발서심의농 
	도덕법칙전성교 
	일월산천만고동

To repay ancient grievances for all time, 
This autumn, we pledge our sincere hearts. 
The teachings of morality and laws are passed down, 
sun, moon, mountains, rivers  are eternally  same.


With great thanks to 
Ga Hwan (가환),  Soo Hyun (수현) 
Kang Han  (강한),  Soo Young (수영) f  
